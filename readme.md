
[![Semantic Versioning](https://img.shields.io/badge/Semantic_Versioning-2.0.0-green)](https://semver.org/)
[![Conventional Commits](https://img.shields.io/badge/Conventional_Commits-1.0.0-FE5196)](https://conventionalcommits.org)<br>
[![Commitizen](https://img.shields.io/badge/Commitizen-conventional_commits-FE5196)](https://github.com/commitizen-tools/commitizen)
[![Semantic Release](https://img.shields.io/badge/Semantic_Release-conventional_commits-FE5196)](https://github.com/semantic-release/semantic-release)

<br>

# ISOA Libs
This subgroup intend to gather all libraries shared across multiple ISOA projects in one place.

<br>

# Guidelines
- All projects MUST respect [Conventional Commit 1.0.0](https://conventionalcommits.org).
- All projects MUST respect [Semantic Versioning 2.0.0](https://semver.org/).
  
- All projects MIGHT use [Commitizen](https://github.com/commitizen-tools/commitizen). Best to use the Python version, can be used for any project, system wide and is already configured to work with Conventional Commits. (req Python >= 3.8)

## Language specificity
### JavaScript
- NPM Packages are created with Gitlab CI/CD and [Semantic Release](https://github.com/semantic-release/semantic-release). (req Node >= 18.0.0)

<br>

# Installation and Configuration
## Commitizen
[Commitizen documentation](https://github.com/commitizen-tools/commitizen). Must be configured to use [Conventional Commit 1.0.0](https://conventionalcommits.org) (default config).
```shell
pip install --user -U Commitizen
```
Basic usage, with `cz commit` is enough (for now).<br>

<br>

## Semantic Release
[Semantic Release documentation](https://github.com/semantic-release/semantic-release). Must be configured to use [Conventional Commit 1.0.0](https://conventionalcommits.org).<br>
**Require Node >= 18.0.0**

1.  Install
```shell
npm install --save-dev semantic-release @semantic-release/git @semantic-release/gitlab conventional-changelog-conventionalcommits
```

2. [Gitlab CI/CD Pipeline](https://docs.gitlab.com/ee/ci/examples/semantic-release.html#configure-the-pipeline)

3. [Gitlab CI/CD Variables](https://docs.gitlab.com/ee/ci/examples/semantic-release.html#set-up-cicd-variables)

4. Add this to `package.json`
```json
{
  "scripts": {
    "semantic-release": "semantic-release"
  },
  "publishConfig": {
    "access": "public"
  },
  "files": [ <path(s) to files here> ]
}
```

5. Create `.releaserc.json` and use Conventional Commits
```json
{
  "branches": ["main"],
  "plugins": [
    [
      "@semantic-release/commit-analyzer",
      {
        "preset": "conventionalcommits"
      }
    ],
    [
      "@semantic-release/release-notes-generator",
      {
        "preset": "conventionalcommits"
      }
    ],
    "@semantic-release/gitlab",
    "@semantic-release/npm",
    [
      "@semantic-release/git",
      {
        "assets": ["package.json"],
        "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
      }
    ]
  ]
}
```
